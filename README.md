# analise-crédito

## Esse projeto disponibiliza endpoints para realizar analise de crédito

## Descrição
 ### Aqui estão a lista de endpoints que podem ser utilizados para manipulação dos dados em JSON:

## Objeto Propostas
* {URL}/analisecredito/propostas/cadastrar    - adiciona propostas
### Exemplo Body Request - POST
----

```json
{
        "nomeCliente": "GUILHERME TESTE",
        "limiteCredito": 300.0,
        "usuario": {
            "id": 2
        },
        "status": {
            "id": 1
        }
    }
```
* {URL}/analisecredito/propostas/atualizar       - autoriza uma proposta (necessário passar o id da proposta, id do usuario e o id do novo status conforme abaixo)
### Exemplo Body Request - PUT
----

```json
   {
        "id": 6,
        "usuario": {
            "id": 1
        },
        "status": {
            "id": 2
        }
    }
```

* {URL}/analisecredito/propostas/listar           - lista todos as propostas (GET)


## Objeto usuario
* {URL}/analisecredito/usuarios/listar -lista todos usuarios

## Informações de apoio
As Informações de usuário, grupousuário e status são inseridas automaticamente quando o ambiente é iniciado com o docker. Segue os dados das entidades:

### GRUPO:
* INSERT INTO `grupo` VALUES (1,'ANALISTA'),(2,'CAPTURADOR');

### STATUSPROPOSTA
* INSERT INTO `statusproposta` VALUES (1,'Em análise'),(2,'Aprovado'),(3,'Negado');

### USUARIO
INSERT INTO `usuario` VALUES (1,'guilherme.santana','Guilherme Santana',1),(2,'rodrigo.alves','Rodrigo Alves',2);
*

 ### Bibliotécas usadas
* Spring MVC (Spring Web)
* Spring Data JPA com Hibernate
* MySQL
* Junit
* Mock
### Ferramentas usadas
* IntellJ
* Docker

### Como compilar?
* Na pasta raiz executar: docker-compose up
