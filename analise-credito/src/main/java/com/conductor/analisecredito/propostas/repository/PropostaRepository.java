package com.conductor.analisecredito.propostas.repository;

import com.conductor.analisecredito.propostas.entity.PropostaCredito;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface PropostaRepository extends JpaRepository<PropostaCredito, Integer> {

}
