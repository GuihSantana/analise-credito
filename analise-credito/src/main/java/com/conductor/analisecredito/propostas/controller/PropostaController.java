package com.conductor.analisecredito.propostas.controller;

import com.conductor.analisecredito.propostas.entity.PropostaCredito;
import com.conductor.analisecredito.propostas.service.PropostaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PropostaController {

    @Autowired
    private PropostaService propostaService;

    @PostMapping("analisecredito/propostas/cadastrar")
    public String cadastrarProsposta(@RequestBody PropostaCredito propostaCredito) throws Exception{
        return propostaService.salvarProposta(propostaCredito);
    }

    @GetMapping("analisecredito/propostas/listar")
    public List<PropostaCredito> listarPropostas(){
        return propostaService.getPropostas();
    }

    @PutMapping("analisecredito/propostas/atualizar")
    public String atualizarProposta(@RequestBody PropostaCredito propostaCredito) throws Exception{
        return propostaService.analisarProposta(propostaCredito);
    }

}
