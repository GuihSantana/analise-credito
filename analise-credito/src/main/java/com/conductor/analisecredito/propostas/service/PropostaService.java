package com.conductor.analisecredito.propostas.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import com.conductor.analisecredito.propostas.entity.PropostaCredito;
import com.conductor.analisecredito.usuarios.entity.Usuario;
import com.conductor.analisecredito.usuarios.service.UsuarioService;
import com.conductor.analisecredito.propostas.repository.PropostaRepository;
import com.conductor.analisecredito.utils.GrupoEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PropostaService {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = new Date(System.currentTimeMillis());

    @Autowired
    private PropostaRepository propostaRepository;

    @Autowired
    UsuarioService usuarioService;

    public String salvarProposta(PropostaCredito proposta) throws Exception {
        try {
            proposta.setDataProposta(date);
            proposta.setDataAtualizacao(date);


            Usuario usuario = usuarioService.consultaUsuário(proposta.getUsuario());
            if (usuario != null) {
                if ((GrupoEnums.CAPTURADOR) == usuario.getGrupo().getId()) {
                    propostaRepository.save(proposta);
                }else{
                    throw new Exception("Exec a ser tratada Usuário não autorizado!");
                }
            }

            return "Salvo com sucesso!";

        } catch (Exception e) {
            throw e;
        }

    }

    public List<PropostaCredito> getPropostas() {
        return propostaRepository.findAll();
    }

    public PropostaCredito getProposta(int id) {
        return propostaRepository.findById(id).orElseThrow(null);
    }

    public String analisarProposta(PropostaCredito propostaCredito) throws Exception{
        try{
            PropostaCredito propostaExistente = propostaRepository.findById(propostaCredito.getId()).orElseThrow(null);
            propostaExistente.setDataAtualizacao(date);
            if (propostaCredito.getUsuario().getId() != null) {
                Usuario usuario = usuarioService.consultaUsuário(propostaCredito.getUsuario());
                if ((GrupoEnums.ANALISTA) == usuario.getGrupo().getId()) {
                    propostaExistente.setUsuario(propostaCredito.getUsuario());
                    propostaExistente.setStatus(propostaCredito.getStatus());
                    propostaRepository.save(propostaExistente);
                }else {
                    throw new Exception("Exc a ser tratada usuario invalido");
                }
            }
            return "Atualizado com sucesso!";
        }catch (Exception e){
            throw e;
        }
    }
}