package com.conductor.analisecredito.usuarios.service;

import com.conductor.analisecredito.usuarios.entity.Usuario;
import com.conductor.analisecredito.usuarios.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario consultaUsuário(Usuario usuario){
        return usuarioRepository.getOne(usuario.getId());
    }

    public List<Usuario> listarUsuarios(){
        return usuarioRepository.findAll();
    }
}
