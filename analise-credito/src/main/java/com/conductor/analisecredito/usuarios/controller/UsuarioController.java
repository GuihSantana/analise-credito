package com.conductor.analisecredito.usuarios.controller;


import com.conductor.analisecredito.usuarios.entity.Usuario;
import com.conductor.analisecredito.usuarios.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("analisecredito/usuarios/listar")
    public List<Usuario> listarUsuario(){
        return usuarioService.listarUsuarios();
    }
}
