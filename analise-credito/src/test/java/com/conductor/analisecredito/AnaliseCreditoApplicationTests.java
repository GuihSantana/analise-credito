package com.conductor.analisecredito;

import com.conductor.analisecredito.usuarios.entity.Usuario;
import com.conductor.analisecredito.usuarios.service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.*;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class AnaliseCreditoApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private UsuarioService usuarioService;


	@Test
	public void testListaUsuario() throws Exception{
		mockMvc.perform(get("/analisecredito/usuarios/listar").accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	@Test
	public void testListaUsuarioJunit() throws Exception{
		List<Usuario> usuarios = usuarioService.listarUsuarios();
		Assertions.assertNotNull(usuarios);

	}

}
