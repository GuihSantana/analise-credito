DROP DATABASE IF EXISTS analisecredito;
CREATE DATABASE analisecredito;
CREATE USER 'guilherme'@'localhost' IDENTIFIED BY 'guilherme';
GRANT ALL PRIVILEGES ON *.* TO 'guilherme'@'%';
GRANT ALL PRIVILEGES ON *.* TO 'guilherme'@'localhost';
FLUSH PRIVILEGES; 


USE analisecredito;


--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grupo` (
  `id` int NOT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,'ANALISTA'),(2,'CAPTURADOR');
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusproposta`
--

DROP TABLE IF EXISTS `statusproposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statusproposta` (
  `id` int NOT NULL,
  `descricao` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusproposta`
--

LOCK TABLES `statusproposta` WRITE;
/*!40000 ALTER TABLE `statusproposta` DISABLE KEYS */;
INSERT INTO `statusproposta` VALUES (1,'Em análise'),(2,'Aprovado'),(3,'Negado');
/*!40000 ALTER TABLE `statusproposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id` int NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `id_grupo` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_id_grupo` (`id_grupo`),
  CONSTRAINT `FK_id_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'guilherme.santana','Guilherme Santana',1),(2,'rodrigo.alves','Rodrigo Alves',2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposta`
--

DROP TABLE IF EXISTS `proposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proposta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome_cliente` varchar(40) DEFAULT NULL,
  `data_proposta` datetime DEFAULT NULL,
  `data_atualizacao` datetime DEFAULT NULL,
  `limite_credito` double DEFAULT NULL,
  `id_usuario` int DEFAULT NULL,
  `id_status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_id_usuario` (`id_usuario`),
  KEY `FK_id_status` (`id_status`),
  CONSTRAINT `FK_id_status` FOREIGN KEY (`id_status`) REFERENCES `statusproposta` (`id`),
  CONSTRAINT `FK_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposta`
--

LOCK TABLES `proposta` WRITE;
/*!40000 ALTER TABLE `proposta` DISABLE KEYS */;
INSERT INTO `proposta` VALUES (1,'GUILHERME TESTE','2021-03-01 18:42:26','2021-03-01 18:42:26',300,2,1);
/*!40000 ALTER TABLE `proposta` ENABLE KEYS */;
UNLOCK TABLES;




/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-01 15:48:29

